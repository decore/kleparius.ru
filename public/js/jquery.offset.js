$(document).ready(function() {
    if ($.browser.webkit) {
        $('a[href*=#]').live('click', function(e) {
            e.preventDefault();
            var target = $(this).attr("href");
            var scrollToPosition = $(target).offset().top;
            $('html, body').stop().animate({ scrollTop: scrollToPosition - 50}, 600, function() {
                // location.hash = target;
                history.pushState({foo: 'bar'}, 'Title', target);
            });
            return false;
        });
    }
    else {
        $('a[href*=#]').live('click', function(e) {
            e.preventDefault();
            var target = $(this).attr("href");
            var scrollToPosition = $(target).offset().top;
            location.hash = target;
            $('html, body').scrollTop(scrollToPosition - 50);
            return false;
        });
    }

    var hash = window.location.hash;
    if (hash) {
        $('a[href^=' + hash + ']').click();
    };
});