$(document).ready(function() {
    var numbers =  $('#style_changer a').size();
    $('#style_changer').css({width: 40 * numbers + 'px'});
    $('#style_changer ul li').css({width: 100 / numbers + '%'});
    $('#style_changer a').click(function () {
        var number_of_style = $(this).html();
        $('link').attr('href', '/css/screen' + number_of_style + '.css');
    });
});